# Buildroot for versatile board with custom HelloWorld package

1. Place Hello to $(BUILDROOT_DIR)/package\
        1. Add package reference to $(BUILDROOT_DIR)/package/Config.in:\
        menu "Custom packages"\
                source "package/Hello/Config.in"\
        endmenu
        
2. Generate .config file\
make O=~/repos/versatile qemu_arm_versatile_defconfig\
        1. cd ~/repos/versatile\
        make menuconfig\
                Build optons -> enable ccache - y\
                Target packages -> Custom packages -> Hello - y\
        make

3. Run system\
./images/start-qemu.sh\
Hello\
Hello world

4. Rebuild package\
make Hello-dirclean\
make 
